# Teste
Um sistema de cadastro simples

## Para utilizar:

### Banco de dados
Para utilizar o cadastro é necessário um schema 'cadastro'.
Já para usar os testes unitários é necessario o schema 'teste'.
Os comandos para a criação destes se encontram no arquivo banco.sql.

O usuário, senha e host do banco MySQL a ser utilizados podem ser configurados nos seguintes arquivos:
Cadastro/config/config.php
Cadastro/config/teste.php

Por padrão, utiliza usuário e senha 'externo' em localhost.

### Comandos para instalar dependências
A única dependência é o phpunit, para testes unitários.
composer install

### Para executar testes
O caminho para o endpoint fake deve ser configurado no seguinte arquivo:
Cadastro/config/teste.php

#### Comando para executar testes:
./vendor/bin/phpunit

## Ambiente usado para testes
Utilizei uma vagrant com Debian 8, MySQL 5.7 e PHP 7.2.

## Sobre algumas decisões de implementação
Adicionei jquery somente para fazer a máscara de números de telefone.
