<?php

require_once(__DIR__ . '/Config/config.php');

function __autoload($class) {
    $exploded = explode('\\', $class);
    if ($exploded[0] == 'Cadastro') {
        unset($exploded[0]);
    }
    $class = implode('/', $exploded) . '.php';
    require_once($class);
}