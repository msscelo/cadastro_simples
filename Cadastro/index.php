<?php

require('autoload.php');

$pessoaBanco = null;
$pessoas = [];
$mensagem = '';

try {
    $pessoaBanco = new Cadastro\Classes\Banco\Pessoa();
    $pessoas = $pessoaBanco->todos();
} catch (Cadastro\Classes\Exceptions\DatabaseProblemException $e) {
    $mensagem = $e->getMessage();
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cadastro</title>
    <link rel="stylesheet" type="text/css" href="storage/cadastro.css">
    <link rel="shortcut icon" type="image/x-icon" href="storage/favicon.ico"/>
</head>

<body>
    <div id="principal">
        <h3>
        Tela inicial
        </h3>
        <a href='novo.php'>Criar Cadastro Novo</a>
        <?php
        if ($mensagem) {
            ?>
            <div id="mensagem">
                <?= $mensagem; ?>
            </div>
            <?php
        }
        if (!$pessoaBanco) {
            ?>
            Erro ao consultar cadastros no banco.
            <?php
        } elseif (empty($pessoas)) {
            ?>
            Nenhum cadastro foi encontrado.
            <?php
        } else {
            ?>
            <ul>
                <?php
                foreach ($pessoas as $atual) {
                    ?>
                <li>
                    Nome: <?= $atual['nome']; ?><br/>
                    Data de envio: <?= $atual['data_criacao']; ?><br/>
                    <?php
                    if (!$atual['envio_ok']) {
                        ?>
                        <div class='erro'>Erro ao enviar para api externa.</div>
                        <?php
                    }
                    ?>
                </li>
                <?php
                }
            ?>
            <ul>
            <?php
        }
        ?>
    </div>
</body>

</html>