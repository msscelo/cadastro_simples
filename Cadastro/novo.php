<?php

require('autoload.php');

$mensagem = '';
try {
    $pessoaBanco = new Cadastro\Classes\Banco\Pessoa();
} catch (Cadastro\Classes\Exceptions\DatabaseProblemException $e) {
    $mensagem = $e->getMessage();
}

$fazCadastro = $_REQUEST['Cadastrar'] ?? false;
if ($fazCadastro) {
    $cadastro = new Cadastro\Classes\Cadastro();
    $resultado = $cadastro->novoCadastro($_REQUEST);
    switch ($resultado) {
        case Cadastro\Classes\Cadastro::SUCESSO:
            $mensagem = 'Cadastro realizado com sucesso';
            break;
        case Cadastro\Classes\Cadastro::FALHA_BANCO:
            $mensagem = 'Não foi possível realizar cadastro, confira os dados digitados';
            break;
        case Cadastro\Classes\Cadastro::FALHA_ENDPOINT:
            $mensagem = 'Falha ao salvar no servidor remoto';
            break;
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cadastro</title>
    <link rel="stylesheet" type="text/css" href="storage/cadastro.css">
    <link rel="shortcut icon" type="image/x-icon" href="storage/favicon.ico"/>
    <script type="text/javascript" src="storage/cadastro.js"></script>
    <script type="text/javascript" src="storage/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="storage/jquery.mask.js"></script>
</head>

<body>
    <div id="principal">
        <h3>
        Cadastro
        </h3>
        <a href='index.php'>Voltar a tela inicial</a>
        <?php
        if ($mensagem) {
            ?>
            <div id="mensagem">
                <?= $mensagem; ?>
            </div>
            <?php
        }
        ?>
        <form action="novo.php" method="post">
            <label for="nome">Nome:</label>
            <input type="text" name="nome" placeholder="Nome" required/><br/>

            <label for="sobrenome">Sobrenome:</label>
            <input type="text" name="sobrenome" placeholder="Sobrenome" required/><br/>

            <label for="email">E-mail:</label>
            <input type="email" name="email" placeholder="E-mail" required/><br/>

            <label for="telefone">Telefone:</label>
            <input type="telefone" name="telefone" id="telefone" placeholder="Telefone" required/><br/>

            <label for="data">Data de Nascimento:</label>
            <input type="date" name="data" id="data"  required/><br/>

            <input type="submit" name="Cadastrar" value="Cadastrar">
       </form>
    </div>
</body>

</html>