<?php

namespace Cadastro\Classes;

use Cadastro\Classes\Externo;
use Cadastro\Classes\Banco\Pessoa;
use Cadastro\Classes\Exceptions\DatabaseProblemException;

class Externo
{
    /**
     * Transforma os dados para json e envia para o servidor remoto
     *
     * @param array $dados
     *
     * @return bool
     */
    public static function salva($dados)
    {
        $dadosJson = self::transformaDados($dados);
        if (empty($dadosJson)) {
            return false;
        }

        return self::sendCurl($dadosJson);
    }

    /**
     * Transforma os dados para json
     *
     * @param array $dados
     *
     * @return string
     */
    private static function transformaDados($dados)
    {
        $obrigatorios = ['nome', 'sobrenome', 'email', 'telefone', 'data'];
        foreach ($obrigatorios as $atual) {
            if (!isset($dados[$atual]) || empty($dados[$atual])) {
                return '';
            }
        }

        return json_encode([
            'nome'       => $dados['nome'],
            'sobre_nome' => $dados['sobrenome'],
            'email'      => $dados['email'],
            'telefone'   => $dados['telefone'],
            'nascimento' => $dados['data'],
        ]);
    }

    /**
     * Envia json para servidor remoto
     *
     * @param string $dadosJson
     *
     * @return bool
     */
    private static function sendCurl($dadosJson)
    {
        $curlObject = curl_init();
        curl_setopt($curlObject, CURLOPT_URL, EXTERNAL_API_ENDPOINT);
        curl_setopt($curlObject, CURLOPT_POST, 1);
        curl_setopt($curlObject, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlObject, CURLOPT_HTTPHEADER, [
            'Content-Type:application/json',
            'Accept: application/json',
            'Credencial: URokYjdzCdLJ',
            'Credencial_pass: LFypMTkuljmJ',
        ]);
        curl_setopt($curlObject, CURLOPT_POSTFIELDS, $dadosJson);
        $resultado = curl_exec($curlObject);
        $infoResposta = curl_getinfo($curlObject);
        curl_close($curlObject);
        $codigoResposta = $infoResposta['http_code'] ?? null;

        return $codigoResposta == 200;
    }


}
