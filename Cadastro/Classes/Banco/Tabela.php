<?php

namespace Cadastro\Classes\Banco;

use Cadastro\Classes\Exceptions\DatabaseProblemException;

/**
 * Classe auxiliar para facilitar o acesso ao banco.
 */
abstract class Tabela
{
    /**
     * SQL de criação de schema.
     */
    private $sqlSchema = "CREATE SCHEMA `%s`";

    private function getSqlSchema()
    {
        return $this->sqlSchema();
    }

    /**
     * Retorna o nome da tabela.
     */
    abstract protected function getNomeTabela();

    /**
     * Retorna o script sql de criação para uma tabela.
     */
    abstract protected function getSqlCriacaoTabela();

    public function __construct()
    {
        $this->checaBanco();
    }

    /**
     * Checa se o banco está acessível e configurado.
     *
     * @throws DatabaseProblemException
     */
    protected function checaBanco()
    {
        $this->db = mysqli_connect(
            DATABASE_HOSTNAME,
            DATABASE_USERNAME,
            DATABASE_PASSWORD,
            DATABASE_DATABASE
        );
        if (empty($this->db)) {
            throw new DatabaseProblemException(
                'Não foi possível conectar no banco de dados. Por favor cheque as configurações e crie schema com o seguinte comando: '
                . sprintf($this->sqlSchema, DATABASE_DATABASE)
            );
        }
        $tabelaExiste = $this->query('SELECT 1 FROM ' . $this->getNomeTabela() . ' LIMIT 1');

        if (empty($tabelaExiste)) {
            $this->criaTabela();
        }
    }

    /**
     * Cria a tabela de tarefas.
     *
     * @throws DatabaseProblemException
     */
    protected function criaTabela()
    {
        $sql = $this->getSqlCriacaoTabela();
        $tabelaCriada = mysqli_query($this->db, $sql);
        if (!$tabelaCriada) {
            throw new DatabaseProblemException(
                'Não foi possivel criar a tabela no banco. Comando sql: ' . $sql
            );
        }
    }

    /**
     * Executa uma query no banco.
     *
     * @param string $query
     *
     * @return array
     */
    public function query($query, $retorno = false)
    {
        $resultado = mysqli_query($this->db, $query);
        if (!$retorno || empty($resultado)) {
            return $resultado;
        }

        return mysqli_fetch_all($resultado);
    }

    public function getInsertId()
    {
        return mysqli_insert_id($this->db);
    }

    /**
     * Remove todos os dados de uma tabela.
     *
     * @return bool
     */
    public function limpa()
    {
        $query = 'DELETE FROM ' . $this->getNomeTabela().' WHERE 1=1';

        return $this->query($query);
    }
}