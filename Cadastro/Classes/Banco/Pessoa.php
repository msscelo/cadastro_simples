<?php

namespace Cadastro\Classes\Banco;

use Cadastro\Classes\Banco\Tabela;
use Cadastro\Classes\Exceptions\DatabaseProblemException;

/**
 * Classe auxiliar para facilitar o uso da tabela pessoas
 */
class Pessoa extends Tabela
{
    /**
     * Retorna o nome da tabela no banco
     */
    protected function getNomeTabela()
    {
        return 'pessoas';
    }

    /**
     * Retorna o script sql de criação da tabela pessoas
     */
    protected function getSqlCriacaoTabela()
    {
        return "CREATE TABLE `" . $this->getNomeTabela() . "` (
            `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            `nome` VARCHAR(255) NOT NULL,
            `sobrenome` VARCHAR(255) NOT NULL,
            `email` VARCHAR(255) NOT NULL,
            `telefone` VARCHAR(16) NOT NULL,
            `data_de_nascimento` DATE NOT NULL,
            `data_criacao` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `envio_ok` BOOL NOT NULL DEFAULT FALSE
        );";
    }

    /**
     * Cria um novo registro no banco.
     *
     * @param string $nome
     * @param string $sobrenome
     * @param string $email
     * @param string $telefone
     * @param string $dataNascimento
     *
     * @return bool
     *
     * @throws DatabaseProblemException
     */
    public function criaPessoa(
        $nome,
        $sobrenome,
        $email,
        $telefone,
        $dataNascimento
    ) {
        if (empty($nome)) {
            $nome = $email;
        }
        // NOTA: vulnerável a sql injection
        $resultado = $this->query(
            "INSERT INTO " . $this->getNomeTabela() . "
                (nome, sobrenome, email, telefone, data_de_nascimento)
            VALUES
                ('{$nome}', '{$sobrenome}', '{$email}', '{$telefone}', '" . $this->formataData($dataNascimento) . "')
            ;"
        );

        if (!$resultado) {
            throw new DatabaseProblemException();
        }

        return $this->getInsertId();
    }

    /**
     * Formata uma data no formato UNIX, adequado ao banco
     *
     * @param $data string
     *
     * @return string
     */
    private function formataData($data)
    {
        $explodido = explode('/', $data);
        if (count($explodido) == 3) {
            $data = implode('-', array_reverse($explodido));
        }

        return $data;
    }

    /**
     * Retorna os dados cadastrados, ordenado por data de cadastro
     *
     * @return array
     */
    public function todos()
    {
        $sql = "SELECT * FROM " . $this->getNomeTabela() . " ORDER BY data_criacao";
        $resultados = $this->query($sql, true);
        if (empty($resultados)) {
            return null;
        }

        $dados = [];
        foreach ($resultados as $atual) {
            $dados[] = [
                'id'                 => $atual[0],
                'nome'               => $atual[1],
                'sobrenome'          => $atual[2],
                'email'              => $atual[3],
                'telefone'           => $atual[4],
                'data_de_nascimento' => $atual[5],
                'data_criacao'       => $atual[6],
                'envio_ok'           => $atual[7],
            ];
        }

        return $dados;
    }

    /**
     * Marca um cadastro como retornado com sucesso do endpoint.
     *
     * @param int $id
     *
     * @return bool
     *
     * @throws DatabaseProblemException
     */
    public function marcaSucesso($id)
    {
        $resultado = $this->query(
            "UPDATE " . $this->getNomeTabela() . "
            SET envio_ok = true
            WHERE id = " . $id . ";"
        );

        if (!$resultado) {
            throw new DatabaseProblemException();
        }

        return $resultado ? true : false;
    }
}
