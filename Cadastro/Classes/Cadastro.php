<?php

namespace Cadastro\Classes;

use Cadastro\Classes\Externo;
use Cadastro\Classes\Banco\Pessoa;
use Cadastro\Classes\Exceptions\DatabaseProblemException;

class Cadastro
{
    private $Pessoa;

    const SUCESSO = 1;
    const FALHA_BANCO = 2;
    const FALHA_ENDPOINT = 3;

    public function __construct()
    {
        $this->Pessoa = new Pessoa();
    }

    public function novoCadastro($dados)
    {
        try {
            $id = $this->Pessoa->criaPessoa(
                $dados['nome'] ?? null,
                $dados['sobrenome'] ?? null,
                $dados['email'] ?? null,
                $dados['telefone'] ?? null,
                $dados['data'] ?? null
            );
        } catch (DatabaseProblemException $e) {
            return self::FALHA_BANCO;
        }

        if (!Externo::salva($dados)) {
            return self::FALHA_ENDPOINT;
        }

        try {
            $this->Pessoa->marcaSucesso($id);
        } catch (DatabaseProblemException $e) {
            return self::FALHA_BANCO;
        }

        return self::SUCESSO;
    }
}
