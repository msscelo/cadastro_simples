<?php

use PHPUnit\Framework\TestCase;

final class PessoaTest extends TestCase
{
    private $pessoa;

    protected function setUp()
    {
        $this->pessoa = new Cadastro\Classes\Banco\Pessoa();
        $this->pessoa->limpa();
    }

    public function testCriaPessoa(): void
    {
        $antes = $this->pessoa->todos();

        $id = $this->pessoa->criaPessoa(
            'nome',
            'sobrenome',
            'email',
            'telefone',
            '01/01/2001'
        );

        $depois = $this->pessoa->todos();
        $this->assertEquals(count($antes) + 1, count($depois));

        $this->assertTrue(!empty($id));

        $ultimo = array_pop($depois);
        $this->assertEquals($ultimo['id'], $id);
        $this->assertEquals($ultimo['sobrenome'], 'sobrenome');
    }

    public function testCriaPessoaInvalida(): void
    {
        $erro = false;
        try {
            $resultado = $this->pessoa->criaPessoa(
                'nome',
                'sobrenome',
                'email',
                'telefone',
                'DATA INVALIDA'
            );
        } catch(Cadastro\Classes\Exceptions\DatabaseProblemException $e) {
            $erro = true;
        }
        $this->assertTrue($erro);
    }

    public function testMarcaSucesso(): void
    {
        $id = $this->pessoa->criaPessoa(
            'nome',
            'sobrenome',
            'email',
            'telefone',
            '01/01/2001'
        );
        $antes = $this->pessoa->todos();
        $this->assertEmpty($antes[0]['envio_ok']);

        $resultado = $this->pessoa->marcaSucesso($id);

        $depois = $this->pessoa->todos();
        $this->assertNotEmpty($depois[0]['envio_ok']);
    }
}
