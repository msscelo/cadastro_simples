<?php

use PHPUnit\Framework\TestCase;
use Cadastro\Classes\Externo;

final class ExternoTest extends TestCase
{
    public function testSalva(): void
    {
        $dadosIncompletos = ['a' => 'b'];
        $retorno = Externo::salva($dadosIncompletos);
        $this->assertFalse($retorno);

        $dados = [
            'nome'      => 'teste',
            'sobrenome' => 'teste',
            'email'     => 'teste',
            'telefone'  => 'teste',
            'data'      => 'teste',
        ];
        $retorno = Externo::salva($dados);
        $this->assertTrue($retorno);
    }
}
