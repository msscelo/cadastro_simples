<?php

use PHPUnit\Framework\TestCase;

final class CadastroTest extends TestCase
{
    private $cadastro;
    private $usuario;

    protected function setUp()
    {
        $this->cadastro = new Cadastro\Classes\Cadastro();
        $this->pessoa   = new Cadastro\Classes\Banco\Pessoa();
    }

    public function testNovoCadastro(): void
    {
        $dados = [
            'nome' => 'nome',
            'sobrenome' => 'sobrenome',
            'email' => 'email',
            'telefone' => 'telefone',
            'data' => '01/01/2001',
        ];

        $retorno = $this->cadastro->novoCadastro($dados);
        $this->assertEquals($retorno, Cadastro\Classes\Cadastro::SUCESSO);

    }

    public function testNovoCadastroFalhaBanco(): void
    {
        $dados = [
            'nome' => 'nome',
            'sobrenome' => 'sobrenome',
            'email' => 'email',
            'telefone' => 'telefone',
            'data' => 'DATA INVALIDA',
        ];

        $retorno = $this->cadastro->novoCadastro($dados);
        $this->assertEquals($retorno, Cadastro\Classes\Cadastro::FALHA_BANCO);
    }

    // TODO teste de falha em endpoint fake
}
