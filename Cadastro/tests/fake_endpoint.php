<?php

// endpoint para propósitos de testes

$dados = json_decode(file_get_contents("php://input"), true);

$headers = getallheaders();
if (
    isset($headers['Content-Type']) && $headers['Content-Type'] == 'application/json' &&
    isset($headers['Accept']) && $headers['Accept'] == 'application/json' &&
    isset($headers['Credencial']) && $headers['Credencial'] == 'URokYjdzCdLJ' &&
    isset($headers['Credencial_pass']) && $headers['Credencial_pass'] == 'LFypMTkuljmJ' &&
    isset($dados['nome']) &&
    isset($dados['sobre_nome']) &&
    isset($dados['email']) &&
    isset($dados['telefone']) &&
    isset($dados['nascimento'])
) {
    return http_response_code(200);
} else {
    return http_response_code(400);
}
